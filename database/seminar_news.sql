/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.9-MariaDB : Database - seminar
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `groups` */

insert  into `groups`(`id`,`name`,`description`) values (1,'admin','Administrator'),(2,'members','General User');

/*Table structure for table `jenjang` */

DROP TABLE IF EXISTS `jenjang`;

CREATE TABLE `jenjang` (
  `id_jenjang` tinyint(2) NOT NULL AUTO_INCREMENT,
  `kode_jenjang` varchar(3) NOT NULL,
  `nama_jenjang` varchar(30) NOT NULL,
  PRIMARY KEY (`id_jenjang`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `jenjang` */

insert  into `jenjang`(`id_jenjang`,`kode_jenjang`,`nama_jenjang`) values (1,'D3','Diploma-3'),(2,'S1','Strata-1');

/*Table structure for table `konsentrasi` */

DROP TABLE IF EXISTS `konsentrasi`;

CREATE TABLE `konsentrasi` (
  `id_konsentrasi` tinyint(2) NOT NULL AUTO_INCREMENT,
  `kode_konsentrasi` varchar(3) NOT NULL,
  `nama_konsentrasi` varchar(30) NOT NULL,
  PRIMARY KEY (`id_konsentrasi`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `konsentrasi` */

insert  into `konsentrasi`(`id_konsentrasi`,`kode_konsentrasi`,`nama_konsentrasi`) values (1,'SI','Sistem Infomasi'),(2,'RPL','Rekayasa Perangkat Lunak'),(3,'TK','Teknik Komputer');

/*Table structure for table `login_attempts` */

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `login_attempts` */

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `id_mahasiswa` int(11) NOT NULL AUTO_INCREMENT,
  `nim` int(8) DEFAULT NULL,
  `nama_mhs` varchar(50) NOT NULL,
  `id_prodi` tinyint(2) DEFAULT NULL,
  `id_konsentrasi` tinyint(2) DEFAULT NULL,
  `id_jenjang` tinyint(2) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  PRIMARY KEY (`id_mahasiswa`),
  KEY `id_prodi` (`id_prodi`,`id_konsentrasi`,`id_jenjang`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8mb4;

/*Data for the table `mahasiswa` */

insert  into `mahasiswa`(`id_mahasiswa`,`nim`,`nama_mhs`,`id_prodi`,`id_konsentrasi`,`id_jenjang`,`email`,`no_telp`,`username`,`password`,`foto`) values (1,12345677,'Mahasiswa 1',1,1,2,'mahasiswa1@gmail.com','0812345678','','',''),(162,12345678,'ninobagus',1,1,1,'ninobagus.mail@gmail.com','089539304966','','',''),(163,NULL,'dodi',NULL,NULL,NULL,'user@email.com','123456789','user','e10adc3949ba59abbe56e057f20f883e','8e3a9d6fb462c5ecdf44ee46f7b6bfe7.jpg'),(164,NULL,'dodi',NULL,NULL,NULL,'example@gmail.com','088789877','test','202cb962ac59075b964b07152d234b70','test.jpg'),(165,NULL,'tested',NULL,NULL,NULL,'tes@gmail.com','088789877','tested','202cb962ac59075b964b07152d234b70','2020122717090734.jpg'),(166,NULL,'dodid',NULL,NULL,NULL,'exam@gmail.com','088789877','tester','202cb962ac59075b964b07152d234b70','202012281530551611.jpg'),(167,NULL,'tes',NULL,NULL,NULL,'tes@mail.co','0887899','tes','202cb962ac59075b964b07152d234b70','202012282227465428.jpg'),(168,NULL,'sss',NULL,NULL,NULL,'sss@gmail.com','08878966','sss','202cb962ac59075b964b07152d234b70','202012291939084901.jpg'),(169,NULL,'seno',NULL,NULL,NULL,'sen@gmail.com','088789955','seno','202cb962ac59075b964b07152d234b70','20201229200405803.jpg'),(170,NULL,'susan',NULL,NULL,NULL,'sus@gmail.com','08553564','susan','202cb962ac59075b964b07152d234b70','202012292010528555.jpg');

/*Table structure for table `metode_pembayaran` */

DROP TABLE IF EXISTS `metode_pembayaran`;

CREATE TABLE `metode_pembayaran` (
  `id_metode` tinyint(2) NOT NULL AUTO_INCREMENT,
  `nama_metode` varchar(15) NOT NULL,
  PRIMARY KEY (`id_metode`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `metode_pembayaran` */

insert  into `metode_pembayaran`(`id_metode`,`nama_metode`) values (1,'Cash'),(2,'Transfer'),(3,'Belum Bayar');

/*Table structure for table `pembicara` */

DROP TABLE IF EXISTS `pembicara`;

CREATE TABLE `pembicara` (
  `id_pembicara` tinyint(3) NOT NULL AUTO_INCREMENT,
  `nama_pembicara` varchar(150) NOT NULL,
  `latar_belakang` varchar(100) NOT NULL,
  `id_seminar` tinyint(3) NOT NULL,
  `foto` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pembicara`),
  KEY `id_seminar` (`id_seminar`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

/*Data for the table `pembicara` */

insert  into `pembicara`(`id_pembicara`,`nama_pembicara`,`latar_belakang`,`id_seminar`,`foto`) values (7,'Rahmat Mulyana ST.,MT.,MBA,PMP,CISA,CISM,CGEIT,CRISC,COBITSF,CSXF,ITLF,ISO27001-LA,ISO9001-IA','President of ISACA Indonesia',1,'8e3a9d6fb462c5ecdf44ee46f7b6bfe7.jpg'),(10,'Prabowo Wahyu Sudarno','Associate Mobile Android Developer in Mamikost',1,'813dfa969b69d1d64f390d34a147f1e7.png'),(11,'Asep Sudarmaji','Area Sales Coordinator Bandung (Grabkios) PT. Grab Indonesia',1,'cb09f1debfecc6df79427b9be30b9d88.png'),(12,'agus','bagas',2,'a37003f9f28af85c6691288a1eed7d38.jpg'),(13,'Mr. DODI','PROGRAMMER',3,'b1043f8a190594167346ca2b7a3001ee.jpg'),(14,'much','guru',4,'92e18935f61f95545ec26c930db81bb9.jpg');

/*Table structure for table `pendaftaran_seminar` */

DROP TABLE IF EXISTS `pendaftaran_seminar`;

CREATE TABLE `pendaftaran_seminar` (
  `id_pendaftaran` int(10) NOT NULL AUTO_INCREMENT,
  `id_seminar` tinyint(3) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  `tgl_daftar` date NOT NULL,
  `jam_daftar` time NOT NULL,
  `id_stsbyr` tinyint(2) NOT NULL,
  `id_metode` tinyint(2) NOT NULL,
  PRIMARY KEY (`id_pendaftaran`),
  KEY `id_seminar` (`id_seminar`,`id_mahasiswa`,`id_stsbyr`,`id_metode`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8mb4;

/*Data for the table `pendaftaran_seminar` */

insert  into `pendaftaran_seminar`(`id_pendaftaran`,`id_seminar`,`id_mahasiswa`,`tgl_daftar`,`jam_daftar`,`id_stsbyr`,`id_metode`) values (159,1,1,'2020-03-20','11:09:07',1,2),(160,1,162,'2020-12-23','09:54:10',2,3),(161,2,165,'2020-12-23','10:22:34',2,3),(162,1,163,'2020-12-26','11:34:57',1,1),(163,1,167,'2020-12-28','12:02:37',1,2),(164,4,165,'2020-12-29','07:57:50',1,2),(165,5,169,'2020-12-29','08:05:40',1,2),(166,4,1,'2020-12-29','08:09:31',1,2),(167,4,170,'2020-12-29','08:11:25',1,1);

/*Table structure for table `presensi_seminar` */

DROP TABLE IF EXISTS `presensi_seminar`;

CREATE TABLE `presensi_seminar` (
  `id_presensi` int(11) NOT NULL AUTO_INCREMENT,
  `id_mahasiswa` int(11) NOT NULL,
  `nomor_induk` int(15) DEFAULT NULL,
  `id_seminar` int(11) NOT NULL,
  `tgl_khd` date NOT NULL,
  `jam_khd` time NOT NULL,
  `id_stskhd` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_presensi`),
  KEY `id_mahasiswa` (`id_mahasiswa`,`id_stskhd`),
  KEY `id_seminar` (`id_seminar`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `presensi_seminar` */

insert  into `presensi_seminar`(`id_presensi`,`id_mahasiswa`,`nomor_induk`,`id_seminar`,`tgl_khd`,`jam_khd`,`id_stskhd`) values (2,165,NULL,1,'2020-12-28','21:41:18',2),(3,165,NULL,2,'2020-12-28','22:24:40',2),(4,170,NULL,4,'2020-12-29','20:12:46',2);

/*Table structure for table `prodi` */

DROP TABLE IF EXISTS `prodi`;

CREATE TABLE `prodi` (
  `id_prodi` tinyint(2) NOT NULL AUTO_INCREMENT,
  `kode_prodi` varchar(2) NOT NULL,
  `nama_prodi` varchar(30) NOT NULL,
  PRIMARY KEY (`id_prodi`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `prodi` */

insert  into `prodi`(`id_prodi`,`kode_prodi`,`nama_prodi`) values (1,'IF','Teknik Informatika'),(2,'MI','Manajemen Informatika'),(3,'KA','Komputer Akuntansi');

/*Table structure for table `seminar` */

DROP TABLE IF EXISTS `seminar`;

CREATE TABLE `seminar` (
  `id_seminar` int(3) NOT NULL AUTO_INCREMENT,
  `nama_seminar` varchar(50) NOT NULL,
  `tgl_pelaksana` date NOT NULL,
  `lampiran` varchar(64) NOT NULL,
  `qrcode` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_seminar`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `seminar` */

insert  into `seminar`(`id_seminar`,`nama_seminar`,`tgl_pelaksana`,`lampiran`,`qrcode`) values (1,'Smart Society In Era 4.0','2020-12-28','190b2b206ed49e3aa214f9f8843b91e0.jpg','16092045811code.png'),(2,'seminar server','2020-12-28','f17fe527f040627c8e1117dc2103583f.jpg','16092045922code.png'),(3,'Seminar Pemrograman PHP','2020-12-30','35ad703d9d74f0bcc321014ba2c16350.jpg','16092043723code.png'),(4,'seminar test','2020-12-29','3cfd681f5c1ef4551438facec34e698f.jpg','16092475524code.png'),(5,'seminar tested','2020-12-30','43eca2aed453ec947cd2b6dc1c366091.jpg','16092461515code.png');

/*Table structure for table `sponsor` */

DROP TABLE IF EXISTS `sponsor`;

CREATE TABLE `sponsor` (
  `id_sponsor` tinyint(3) NOT NULL AUTO_INCREMENT,
  `nama_sponsor` varchar(30) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `id_seminar` tinyint(3) NOT NULL,
  PRIMARY KEY (`id_sponsor`),
  KEY `id_seminar` (`id_seminar`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sponsor` */

insert  into `sponsor`(`id_sponsor`,`nama_sponsor`,`gambar`,`id_seminar`) values (2,'Nutragen','503e56f7e0818aada74a6005adcb47de.jpg',1),(3,'Sosro','e61859bd372d0d074b94c35262f16fd6.jpg',1),(4,'Tiesore','6af1ad9e4af93a7f1936131687d0f515.jpg',1),(5,'Tri Cell','de6d8eee332e2a452b5f5ff6a495d2d5.jpg',1),(6,'Binary Indonesia','63b611e06c384c798e5c6ffc8cf9eb31.jpg',1),(7,'MAGER','80b9fdf5cc16086b293f09c4dbb8aa4b.jpg',3),(8,'pastagigi','a440cb70fe514c1e24363dd2e23ee6e2.jpg',4);

/*Table structure for table `status_kehadiran` */

DROP TABLE IF EXISTS `status_kehadiran`;

CREATE TABLE `status_kehadiran` (
  `id_stskhd` tinyint(1) NOT NULL AUTO_INCREMENT,
  `nama_stskhd` varchar(15) NOT NULL,
  PRIMARY KEY (`id_stskhd`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `status_kehadiran` */

insert  into `status_kehadiran`(`id_stskhd`,`nama_stskhd`) values (1,'Tidak Hadir'),(2,'Hadir');

/*Table structure for table `status_pembayaran` */

DROP TABLE IF EXISTS `status_pembayaran`;

CREATE TABLE `status_pembayaran` (
  `id_stsbyr` tinyint(2) NOT NULL AUTO_INCREMENT,
  `nama_stsbyr` varchar(15) NOT NULL,
  PRIMARY KEY (`id_stsbyr`),
  KEY `nama_stsbyr` (`nama_stsbyr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `status_pembayaran` */

insert  into `status_pembayaran`(`id_stsbyr`,`nama_stsbyr`) values (2,'Belum Lunas'),(1,'Sudah Lunas');

/*Table structure for table `tiket` */

DROP TABLE IF EXISTS `tiket`;

CREATE TABLE `tiket` (
  `id_tiket` tinyint(3) NOT NULL AUTO_INCREMENT,
  `id_seminar` tinyint(3) NOT NULL,
  `harga_tiket` bigint(15) NOT NULL,
  `slot_tiket` int(5) NOT NULL,
  `lampiran_tiket` varchar(100) NOT NULL,
  PRIMARY KEY (`id_tiket`),
  KEY `id_seminar` (`id_seminar`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tiket` */

insert  into `tiket`(`id_tiket`,`id_seminar`,`harga_tiket`,`slot_tiket`,`lampiran_tiket`) values (4,1,35000,200,'4fe1870a3764ad3cb7fd0a5e22363b8d.jpg'),(5,2,4000,10,'67166adeafa5563b0a16628eb7bfc5e6.jpg'),(6,3,100000,100,'5a8c325c5b3dbe982746217333cde2eb.jpg'),(7,4,50000,50,'5a8c325c5b3dbe982746217333cde2eb.jpg'),(8,5,25000,45,'5a8c325c5b3dbe982746217333cde2eb.jpg');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `foto` varchar(50) NOT NULL,
  `is_mobile` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_email` (`email`),
  UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  UNIQUE KEY `uc_remember_selector` (`remember_selector`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`ip_address`,`username`,`password`,`email`,`activation_selector`,`activation_code`,`forgotten_password_selector`,`forgotten_password_code`,`forgotten_password_time`,`remember_selector`,`remember_code`,`created_on`,`last_login`,`active`,`first_name`,`last_name`,`company`,`phone`,`foto`,`is_mobile`) values (1,'127.0.0.1','administrator','$2y$12$ml83HGMMj9/8lQ9yTVmhrO/jIQiGPEwvn0cIXYRP2W3fIaUHeo7G.','admin@admin.com',NULL,'',NULL,NULL,NULL,NULL,NULL,1268889823,1609245605,1,'Admin','istrator','ADMIN','0','',0),(7,'','user','e10adc3949ba59abbe56e057f20f883e','user@email.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'dodi','dido',NULL,'123456789','8e3a9d6fb462c5ecdf44ee46f7b6bfe7.jpg',1);

/*Table structure for table `users_groups` */

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `users_groups` */

insert  into `users_groups`(`id`,`user_id`,`group_id`) values (1,1,1),(2,1,2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
