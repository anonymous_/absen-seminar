<?php
if (isset($_SERVER['HTTP_ORIGIN'])){
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Max-Age: 86400");
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS'){
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

 
require APPPATH . '/libraries/REST_Controller.php';
 
class CheckPass extends REST_Controller {
 
    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('M_Api');
    }
 
    // insert
    public function index_post() {

    	$id 			= base64_decode($this->post('iduser'));
    	$oldpassword 	= md5($this->post('oldpassword'));
    	$level			= $this->post('level');
        $result 		= '';
        $check 			= '';
        $list = array();

        $data = array(
        	'id'		=> $id,
            'password'	=> $oldpassword
        );

        if ($level == 'Warga') {
        	# code...
        	$check = $this->M_Api->getwhere('warga', $data);
        } else {
        	$check = $this->M_Api->getwhere('polisi', $data);
        }

        if (count($check) == 1) {
        	# code...
        	$result_data = array('success'=>'Success');
            $list[] = $result_data;
            $result = array(
                "results" => $list
            );
            $this->set_response($result, REST_Controller::HTTP_CREATED);
        } else {
        	$result_data = array('success'=>'Failed');
            $list[] = $result_data;
            $result = array(
                "results" => $list
            );
            $this->set_response($result, REST_Controller::HTTP_CREATED);
        }

        
    }
 
}