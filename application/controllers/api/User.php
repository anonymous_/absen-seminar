<?php
if (isset($_SERVER['HTTP_ORIGIN'])){
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header("Access-Control-Allow-Credentials: true");
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS'){
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
// header('Content-type: application/json');
// header("Access-Control-Allow-Origin: *");
// header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller {

  function __construct($config = 'rest') {
    parent::__construct($config);
    $this->load->model('M_Api');
    $this->load->model(
        [
            'Seminar_model' => 'sm',
            'Scan_model' => 'sc',
        ]
    );
  }

  function registrasi_post()
  {
    $foto_name = date('YmdHis').rand(4, 9999).'.jpg';
    $data = array(
      'nama_mhs'  => $this->post('name'),
      'email'     => $this->post('email'),
      'no_telp'   => $this->post('phone'),
      'username'  => $this->post('username'),
      'password'  => md5($this->post('password')),
      'foto'      => $foto_name
    );

    // check_username
    $check_username = $this->M_Api->getwhere('mahasiswa', ['username'=>$this->post('username')]);
    $check_email = $this->M_Api->getwhere('mahasiswa', ['email'=>$this->post('email')]);

    if (count($check_username) > 0) {
      $result_data[] = array(
        'error'   => false,
        'status'  => 'failed',
        'message' => 'Username sudah digunakan'
      );
      $result = array(
          "results" => $result_data
      );
      $this->set_response($result, REST_Controller::HTTP_CREATED);
    } else if (count($check_email) > 0) {
      $result_data[] = array(
        'error'   => false,
        'status'  => 'failed',
        'message' => 'Email sudah digunakan'
      );
      $result = array(
          "results" => $result_data
      );
      $this->set_response($result, REST_Controller::HTTP_CREATED);
    } else {
      if ($this->M_Api->insert('mahasiswa', $data)) {
        $path = "uploads/profile";
        if (!is_dir($path)) {
          mkdir($path, 0777, true);
        }

        file_put_contents($path."/".$foto_name, base64_decode($this->post('foto')));

        $result_data[] = array(
          'error'   => false,
          'status'  => 'success',
          'message' => 'Pendaftaran Berhasil'
        );
        $result = array(
            "results" => $result_data
        );
        $this->set_response($result, REST_Controller::HTTP_CREATED);
      } else {
        $result_data[] = array(
          'error'   => true,
          'status'  => 'failed',
          'message' => 'Terjadi kesalahan sistem'
        );
        $result = array(
            "results" => $result_data
        );
        $this->set_response($result, REST_Controller::HTTP_CREATED);
      }
    }
  }

  function login_post() {
    $data = array(
      'username' => $this->post('username'), 
      'password' => md5($this->post('password'))
    );

    $query = $this->M_Api->getwhere('mahasiswa', $data);

    $list = array();
    $result = '';
    if (count($query) > 0) {
      foreach ($query as $data_result) {
        $user_data = array(
          'id_mahasiswa'=> $data_result->id_mahasiswa,
          'username'    => $data_result->username,
          'name'        => $data_result->nama_mhs,
          'email'       => $data_result->email,
          'phone'       => $data_result->no_telp,
          'foto'        => $data_result->foto,
          'success'     => 'Success'
        );
      }
      $result_data[] = array(
        'error'   => false,
        'status'  => 'success',
        'message' => 'Login Berhasil',
        'data'    => $user_data
      );
      $result = array(
          "results" => $result_data
      );
      $this->set_response($result, REST_Controller::HTTP_CREATED);
    }else{
      $result_data[] = array(
        'error'   => false,
        'status'  => 'failed',
        'message' => 'User tidak ditemukan'
      );
      $result = array(
          "results" => $result_data
      );
      $this->set_response($result, REST_Controller::HTTP_CREATED);
    }
    
  }

  function absensi_post()
  {
    $id = $this->post('id_mahasiswa');
    $seminar =  $this->post('seminar');
    $tgl = date('Y-m-d');
    $jam = date('H:i:s');
    $cek_id = $this->sc->cek_mhs($id, $seminar);
    $cek_smn = $this->sc->cek_seminar($seminar);
    $cek_khd = $this->sc->cek_kehadiran($id, $seminar);
    if (!$cek_id) {
      $result_data[] = array(
        'error'   => false,
        'status'  => 'failed',
        'message' => 'User belum terdaftar di seminar'
      );
      $result = array(
          "results" => $result_data
      );
      $this->set_response($result, REST_Controller::HTTP_CREATED);
    }else if (!$cek_smn) {
      $result_data[] = array(
        'error'   => false,
        'status'  => 'failed',
        'message' => 'Seminar tidak ditemukan'
      );
      $result = array(
          "results" => $result_data
      );
      $this->set_response($result, REST_Controller::HTTP_CREATED);
    }else if ($cek_smn && $cek_smn->tgl_pelaksana != $tgl) {
      $result_data[] = array(
        'error'   => false,
        'status'  => 'failed',
        'message' => 'Seminar tidak sendang dilaksanakan'
      );
      $result = array(
          "results" => $result_data
      );
      $this->set_response($result, REST_Controller::HTTP_CREATED);
    } else if ($cek_khd && $cek_khd->id_stskhd == 2) {
        $id_mahasiswa = $cek_khd->id_mahasiswa;
        $prodi = $cek_khd->nama_prodi;
        $konsentrasi = $cek_khd->nama_konsentrasi;
        $jenjang = $cek_khd->nama_jenjang;
        $nama_mhs = $cek_khd->nama_mhs;
        $email = $cek_khd->email;
        $no_telp = $cek_khd->no_telp;
        $row = array(
            'id_mahasiswa'  => $id_mahasiswa,
            'tgl_khd'       => $tgl,
            'jam_khd'       => $jam,
            'prodi'         => $prodi,
            'konsentrasi'   => $konsentrasi,
            'jenjang'       => $jenjang,
            'nama_mhs'      => $nama_mhs,
            'email'         => $email,
            'no_telp'       => $no_telp,
        );

      $result_data[] = array(
        'error'   => false,
        'status'  => 'warning',
        'message' => 'Anda sudah melakukan absensi',
        'data'    => $row
      );
      $result = array(
          "results" => $result_data
      );
      $this->set_response($result, REST_Controller::HTTP_CREATED);
    } else {
        $id_mahasiswa = $cek_id->id_mahasiswa;
        $nomor_induk = $cek_id->nim;
        $prodi = $cek_id->nama_prodi;
        $konsentrasi = $cek_id->nama_konsentrasi;
        $jenjang = $cek_id->nama_jenjang;
        $nama_mhs = $cek_id->nama_mhs;
        $email = $cek_id->email;
        $no_telp = $cek_id->no_telp;
        $data = array(
            'id_mahasiswa' => $id_mahasiswa,
            'nomor_induk' => $nomor_induk,
            'id_seminar' => $seminar,
            'tgl_khd' => $tgl,
            'jam_khd' => $jam,
            'id_stskhd' => 2,
        );

        $row = array(
            'nomor_induk' => $nomor_induk,
            'tgl_khd' => $tgl,
            'jam_khd' => $jam,
            'prodi' => $prodi,
            'konsentrasi' => $konsentrasi,
            'jenjang' => $jenjang,
            'nama_mhs' => $nama_mhs,
            'email' => $email,
            'no_telp' => $no_telp,
        );

        $this->sc->insert_data($data);

        $result_data[] = array(
          'error'   => false,
          'status'  => 'success',
          'message' => 'Selamat Datang',
          'data'    => [$row, $cek_id]
        );
        $result = array(
            "results" => $result_data
        );
        $this->set_response($result, REST_Controller::HTTP_CREATED);
    }
  }

  function history_get($id_mahasiswa=null)
  {
    $data = $this->sm->getHistory($id_mahasiswa);

    if (count($data) > 0) {
      foreach ($data as $key => $value) {
        $history[] = array(
          'id_mahasiswa'  => $value->id_mahasiswa,
          'nim'           => $value->nim,
          'nama_mahasiswa'=> $value->nama_mhs,
          'email'         => $value->email,
          'no_telp'       => $value->no_telp,
          'foto'          => $value->foto,
          'id_seminar'    => $value->id_seminar,
          'nama_seminar'  => $value->nama_seminar,
          'tgl_pelaksana' => $value->tgl_pelaksana,
          'lampiran'      => $value->lampiran,
          'qrcode'        => $value->qrcode,
          'id_presensi'   => $value->id_presensi,
          'tanggal_kehadiran'=> $value->tgl_khd,
          'jam_kehadiran'    => $value->jam_khd,
          'nama_status_kehadiran'=> $value->nama_stskhd,
        );
      }

      $result_data[] = array(
        'error'   => false,
        'status'  => 'success',
        'data'    => $history
      );
      $result = array(
          "results" => $result_data
      );
      $this->set_response($result, REST_Controller::HTTP_CREATED);
    } else {
      $result_data[] = array(
        'error'   => false,
        'status'  => 'failed',
        'message' => 'Data tidak di temukan'
      );
      $result = array(
          "results" => $result_data
      );
      $this->set_response($result, REST_Controller::HTTP_CREATED);
    }
  }

}