<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Api extends CI_Model {

	public function __construct()
	{
		// parent::__construct();
		$this->load->database();
	}

	//FUNCTION UNTUK INSERT
	function insert($table, $data) {
		return $this->db->insert($table, $data);
	}

	//FUNCTION UNTUK GET
	function get($table) {
		return $this->db->get($table)->result();
	}

	//FUNCTION UNTUK GET WHERE
  	function getwhere($table, $key)
  	{
  		return $this->db->get_where($table, $key)->result();
  	}

  	//FUNCTION UNTUK UPDATE
  	function update($primary, $id, $table, $data) {
  		$this->db->where($primary, $id);
  		return $this->db->update($table, $data);
  	}
}